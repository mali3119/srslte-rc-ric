#!/bin/sh

cd build

cmake ../ \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DRIC_GENERATED_E2AP_BINDING_DIR=/local/setup/E2AP-v02.03 \
    -DRIC_GENERATED_E2SM_KPM_BINDING_DIR=/local/setup/E2SM-KPM \
    -DRIC_GENERATED_E2SM_NI_BINDING_DIR=/local/setup/E2SM-NI \
    -DRIC_GENERATED_E2SM_GNB_NRT_BINDING_DIR=/local/setup/E2SM-GNB-NRT
    
sudo make -j32
sudo make install

sudo ldconfig
